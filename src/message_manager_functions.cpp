#include "message_manager.hpp"
// #include "beast.hpp"
#include "message_header.hpp"
// #include "session.hpp"
#include <iostream>
// #include <memory>
// #include <net/if.h>
#include <string>
#include <vector>

void message_manager::return_text_x_time(std::vector<std::string> &vector_,
                                         session *sender) {
  // funtion takes
  // first param , time of rebeat
  // second param , text to rebeat
  // 3th param , split char
  //
  std::cout << "inter func\n";
  std::string out_text;

  int end_rebeat = stoi(vector_[0]);

  // print_items_vector(vector_);
  for (int i = 0; i < end_rebeat; i++) {
    out_text += vector_[1] + vector_[2];
  }
  // std::cout << out_text;

  // set message in vector
  std::vector<std::string> send_vector{server_to_client_header::print_cout,
                                       out_text};

  std::cout << send_vector[0] << " - " << send_vector[1] << '\n';

  // send message after convert to sendable style
  sender->send(boost::make_shared<std::string const>(
      vector_to_text_container(send_vector)));
}

void message_manager::print_items_vector(std::vector<std::string> vector_) {

  std::cout << "items in vector\n";
  for (auto i : vector_) {
    std::cout << i << "\n";
  }
}
