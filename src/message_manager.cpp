#include "message_manager.hpp"
// #include "beast.hpp"
#include "message_header.hpp"
#include "session.hpp"
#include <iostream>
#include <memory>
#include <net/if.h>
#include <string>
#include <vector>

#define SPACE_LENGTH 1

#define DEBUG

std::mutex message_manager::mutex_;

// pointer of obj
std::unique_ptr<message_manager> message_manager::p_message_manager = nullptr;

// funtions

void message_manager::create_instance() {

  // to avoid data race
  std::lock_guard<std::mutex> lock(message_manager::mutex_);

  if (message_manager::p_message_manager == nullptr) {

    message_manager::p_message_manager = std::make_unique<message_manager>();
#ifdef DEBUG
    std::cout << "new one\n";
#endif
  }
  // return *message_manager::p_message_manager;
}

// construtor
message_manager::message_manager() {
#ifdef DEBUG
  std::cout << "create instance\n";
#endif // DEBUG
};

message_manager::~message_manager(){};

std::string
message_manager::vector_to_text_container(std::vector<std::string> &vector_) {

  if (vector_.empty()) {
    return "";
  }

  std::string text_ = std::to_string(vector_.size()) + " ";

  for (auto element : vector_) {
    text_ += std::to_string(element.size()) + " " + element + " ";

    //  text_ += std::to_string(element.size()) + element;
    //  add feature remove not important spaces
    //  2 1 !2 aa
    //  like this
  }

  return text_;
}
bool message_manager::message_analysis(const std::string &message,
                                       std::vector<std::string> &vector_param) {
  // std::cout << message << "\n";
  // for parts of message
  // std::vector<std::string> vector_param;

  // get num of parameter
  // message like this
  // 2 5 aaaaa 3 aaa
  // num of param / num of first param / first param message / num of second
  // param ....

  try {

    size_t start_index = 0, end_index = 0, param_len = 0;
    end_index = message.find(" ", start_index);
    // first num in string
    int num_of_param =
        std::stoi(message.substr(start_index, (end_index - start_index)));
    // std::cout << "num of param = " << num_of_param << std::endl;

    for (int i = 0; i < num_of_param; i++) {

      start_index = end_index + param_len + SPACE_LENGTH;
      end_index = message.find(" ", start_index) + SPACE_LENGTH;

      // num of len parameter
      param_len =
          std::stoi(message.substr(start_index, (end_index - start_index)));

      // put param in vector
      vector_param.push_back(message.substr(end_index, param_len));

#ifdef DEBUG

      std::cout << "param is :" << vector_param.back() << "\n";
#endif // DEBUG
    }

    // find_func(vector_param);
    return true;

  } catch (const std::exception &e) {

    // std::string c = "error your format is not correct \n" + message;
    // sender->send(boost::make_shared<std::string const>(c));
    return false;
  }
  // #ifdef DEBUG
  //
  //   std::cout << "items in vector\n";
  //   for (auto i : vector_param) {
  //     std::cout << i << "\n";
  //   }
  // #endif // DEBUG
}

void message_manager::find_func(const std::string message, session *sender) {

  std::vector<std::string> vector_;

  if (!(message_analysis(message, vector_))) {
    // error message
    std::string c = "error your format is not correct \n" + message;
    std::vector<std::string> message_v = {server_to_client_header::print_cout,
                                          c};

    // send as print message
    sender->send(boost::make_shared<std::string const>(
        vector_to_text_container(message_v)));

    return;
  }

  if (vector_.empty()) {
    // std::cout << "header func" << vector_[0] << "\n";
    std::cout << "empty vector";
    return;
  }
  //
  // #ifdef DEBUG
  //
  //   std::cout << "after convert to sting again "
  //             << vector_to_text_container(vector_) << "\n";
  // #endif // DEBUG
  //
  std::cout << "-" << vector_[0] << "\n";
  if (vector_[0] == client_to_server_header::resend_text_x_time) {
    vector_.erase(vector_.begin());
    return_text_x_time(vector_, sender);
  }
}
