#include "session.hpp"
// #include "message_header.hpp"
#include "message_manager.hpp"
#include <iostream>
#include <string>
#include <unistd.h>

#define DEBUG
//------------------------------------------------------------------------------

// Report a failure
void fail(beast::error_code ec, char const *what) {
  std::cerr << what << ": " << ec.message() << "\n";
}

std::unordered_set<session *> session::sessions_;
std::mutex session::mutex_;

// Take ownership of the socket
session::session(tcp::socket &&socket) : ws_(std::move(socket)) {}
session::~session() {

  std::lock_guard<std::mutex> lock(session::mutex_);
  session::sessions_.erase(this);

#ifdef DEBUG
  std::cout << "remove session " << session::sessions_.size() << "\n";
#endif // DEBUG
}

// Get on the correct executor
void session::run() {
  // We need to be executing within a strand to perform async operations
  // on the I/O objects in this session. Although not strictly necessary
  // for single-threaded contexts, this example code is written to be
  // thread-safe by default.
  net::dispatch(ws_.get_executor(), beast::bind_front_handler(
                                        &session::on_run, shared_from_this()));
}

// Start the asynchronous operation
void session::on_run() {
  // Set suggested timeout settings for the websocket
  ws_.set_option(
      websocket::stream_base::timeout::suggested(beast::role_type::server));

  // Set a decorator to change the Server of the handshake
  ws_.set_option(
      websocket::stream_base::decorator([](websocket::response_type &res) {
        res.set(http::field::server, std::string(BOOST_BEAST_VERSION_STRING) +
                                         " websocket-server-async");
      }));
  // Accept the websocket handshake
  ws_.async_accept(
      beast::bind_front_handler(&session::on_accept, shared_from_this()));
}

void session::on_accept(beast::error_code ec) {
  if (ec)
    return fail(ec, "accept");

  {

    std::lock_guard<std::mutex> lock(session::mutex_);
    session::sessions_.insert(this);

#ifdef DEBUG
    std::cout << "add sesson\n num of session " << session::sessions_.size()
              << "\n";
#endif // DEBUG

    // Read a message
    do_read();
  }
}

void session::do_read() {

#ifdef DEBUG
  // send first message after connect
  // send(boost::make_shared<std::string const>("hi"));
  // send(
  // boost::make_shared<std::string const>(static_cast<std::string>(&m) + &c));
#endif
  // Read a message into our buffer
  ws_.async_read(buffer_, beast::bind_front_handler(&session::on_read,
                                                    shared_from_this()));
}

void session::on_read(beast::error_code ec, std::size_t bytes_transferred) {
  boost::ignore_unused(bytes_transferred);

  // This indicates that the session was closed
  if (ec == websocket::error::closed)
    return;

  if (ec)
    return fail(ec, "read");

#ifdef DEBUG
  // get text from buuffer_
  std::cout << beast::buffers_to_string(buffer_.data()) << "\n";
  // Echo the message
  ws_.text(ws_.got_text());
  // std::cout << "got text = " << ws_.got_text() << " got binary "
  //           << ws_.got_binary() << '\n';
#endif

  // save const string gets from buffer
  // way do this to not copy text every call funtion
  auto const ss = boost::make_shared<std::string const>(
      beast::buffers_to_string(buffer_.data()));

  // not need now
  // Clear the buffer
  buffer_.consume(buffer_.size());

#ifdef DEBUG
  // put funtion to message manager here
  // for (int i = 0; i < 1; i++)
  //   send(ss);

  message_manager::p_message_manager->find_func(*ss, this);
#endif // DEBUG

  // do ready again
  do_read();
}

void session::send(boost::shared_ptr<std::string const> const &ss) {
  // Post our work to the strand, this ensures
  // that the members of `this` will not be
  // accessed concurrently.

  net::post(ws_.get_executor(), beast::bind_front_handler(
                                    &session::on_send, shared_from_this(), ss));
}

void session::on_send(boost::shared_ptr<std::string const> const &ss) {
  // Always add to queue0
  queue0_.push_back(ss);

  // Are we already writing?
  if (queue0_.size() > 1) {
#ifdef DEBUG
    // std::cout << "there is " << queue0_.size() << "\n";
#endif // DEBUG

    return;
  }

#ifdef DEBUG
  // std::cout << "first time\n";
#endif // DEBUG

  //  We are not currently writing, so send this immediately
  ws_.async_write(
      net::buffer(*queue0_.front()),
      beast::bind_front_handler(&session::on_write, shared_from_this()));
}

void session::on_write(beast::error_code ec, std::size_t) {
#ifdef DEBUG
  // std::cout << "after send\n";
#endif // DEBUG

  // Handle the error, if any
  if (ec)
    return fail(ec, "write");

  // Remove the string from the queue0
  queue0_.erase(queue0_.begin());

  // Send the next message if any
  if (!queue0_.empty())
    ws_.async_write(
        net::buffer(*queue0_.front()),
        beast::bind_front_handler(&session::on_write, shared_from_this()));
}
