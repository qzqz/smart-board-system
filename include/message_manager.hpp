#ifndef MESSAGE_MANAGER
#define MESSAGE_MANAGER

// Echoes back all received WebSocket messages
#include "session.hpp"

class message_manager {

private:
  static std::mutex mutex_;

  bool message_analysis(const std::string &message,
                        std::vector<std::string> &vector_param);

public:
  static void create_instance();

  message_manager();
  ~message_manager();

  static std::unique_ptr<message_manager> p_message_manager;

  // to not create object to var
  message_manager(message_manager &other) = delete;
  void operator=(const message_manager &) = delete;

  // static message_manager &instance;

  std::string vector_to_text_container(std::vector<std::string> &vector_);

  // after analysis

  void find_func(const std::string message,
                 session *sender); // find funtion that message want

private:
  // -------------------------------------------------------
  // funtion from message here
  // -------------------------------------------------------

  // this for client delete it
  // void print_cout(std::vector<std::string> vector_);
  void print_items_vector(std::vector<std::string> vector_);
  void return_text_x_time(std::vector<std::string> &vector_, session *sender);
};

#endif
