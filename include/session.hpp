#ifndef SESSION_HPP
#define SESSION_HPP

#include "beast.hpp"
#include "net.hpp"
#include <unordered_set>

// Report a failure
void fail(beast::error_code ec, char const *what);

// Echoes back all received WebSocket messages
class session : public std::enable_shared_from_this<session> {

  websocket::stream<beast::tcp_stream> ws_;
  beast::flat_buffer buffer_;

  // 0 for most important messages (notification system messages )
  // 1 for less important messages (images videos message)
  std::vector<boost::shared_ptr<std::string const>> queue0_, queue1_;

  // Keep a list of all the connected clients
  static std::unordered_set<session *> sessions_;

  // This mutex synchronizes all access to sessions_
  static std::mutex mutex_;

public:
  // Take ownership of the socket
  explicit session(tcp::socket &&socket);
  ~session();

  // Get on the correct executor
  void run();

  // Start the asynchronous operation
  void on_run();

  void on_accept(beast::error_code ec);
  void do_read();
  void on_read(beast::error_code ec, std::size_t bytes_transferred);
  void on_write(beast::error_code ec, std::size_t bytes_transferred);

  void send(boost::shared_ptr<std::string const> const &ss);
  void on_send(boost::shared_ptr<std::string const> const &ss);
};

#endif
