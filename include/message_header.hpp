#ifndef MESSAGE_HEADER
#define MESSAGE_HEADER

// header of message that use for find function for all message send

#include <string>
namespace server_to_client_header {

// that will send to client and client will print it on cli
const std::string print_cout = std::string(1, char(33));
} // namespace server_to_client_header

// --------------------------------------------

namespace client_to_server_header {

const std::string resend_text_x_time = std::string(1, char(33));
} // namespace client_to_server_header

#endif

// if u wand to use more than one char
// change in find func
// change in all send func
